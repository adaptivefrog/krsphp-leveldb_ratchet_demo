<?php

use Ratchet\App;
use MyApp\Demo;

require dirname(__DIR__) . '/vendor/autoload.php';


$demo = new Demo();




$loop = \React\EventLoop\Factory::create();
$app = new \Ratchet\App('localhost', 8080, '127.0.0.1', $loop);
$app->route('/',$demo,['*']);




$loop->addPeriodicTimer(5, function($timer) use ($demo) {
	$demo->shout("Hrello, every 30 seconds");
});



$app->run();