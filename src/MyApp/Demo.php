<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use LevelDB;

class Demo implements MessageComponentInterface {
	protected $clients;

	public function __construct() {
		echo "Constructing ";
		$this->clients = new \SplObjectStorage;

		$this->db = new LevelDB("/Users/sverre/ws/tests/ratchetdemo/ldb-test-db");
	}

	public function onOpen(ConnectionInterface $conn) {
		// Store the new connection to send messages to later
		$this->clients->attach($conn);

		echo "New connection! ({$conn->resourceId})\n";
	}

	public function onMessage(ConnectionInterface $from, $msg) {
		$msgObj = json_decode($msg);
		if($msgObj->action =="put") {
			$this->db->put($msgObj->key,$msgObj->value);
		}elseif($msgObj->action =="get") {
			$from->send($this->db->get($msgObj->key));
		}elseif($msgObj->action =="delete") {
			$from->send($this->db->delete($msgObj->key));
		}elseif($msgObj->action =="shout") {
			foreach ($this->clients as $client) {
				if ($from !== $client) {
					// The sender is not the receiver, send to each client connected
					$client->send($msg);
				}
			}
		}



	}

	public function onClose(ConnectionInterface $conn) {
		// The connection is closed, remove it, as we can no longer send it messages
		$this->clients->detach($conn);

		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}
	
	public function shout($message){
		echo "Shouting";
		foreach ($this->clients as $client) {
			$client->send($message);
		}
	}
}